const errors = document.getElementById("errors");

const addError = msg => {
    const returnError = msg => {
        return `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                  ${msg}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>`;
    };
    errors.innerHTML += returnError(msg);
};

const mapTodos = async page => {
    try {
        // Performing the request
        const pagination = page ? `?page=${page}` : "";
        const request = await fetch(`/api/todos${pagination}`);
        const response = await request.json();

        // Fetching the todos list
        const todos = response.data;
        const listUl = document.getElementById("todo-list");

        // Assigning the todos to the ul element
        const items = todos.map(
            ({ name, id }) => `<li class="d-flex align-items-center list-group-item">${name}
            <button class="ml-auto btn btn-danger" onclick="removeTodo(${id})">Remove</button></li>`
        );
        listUl.innerHTML = items.join("");

        // Fetching the pagination data
        const lastPage = response.last_page;
        const currentPage = response.current_page;
        const paginationNav = document.getElementById("pagination-nav");

        // Assigning the pagination buttons
        const getPagHTML = (lastPage, currentPage) => {
            let html = "";
            for (let i = 1; i <= lastPage; i++) {
                active = currentPage === i ? " active" : "";
                html += `<li class="page-item${active}"><a class="page-link" onclick="mapTodos(${i})" href="#">${i}</a></li>`;
            }
            return html;
        };

        const paginationBtns = getPagHTML(lastPage, currentPage);
        paginationNav.innerHTML = paginationBtns;
    } catch (error) {
        addError("Could not retreive todos");
    }
};

const addTodo = e => {
    e.preventDefault();
    const todo = e.target.todo.value;

    fetch("/api/todos", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ name: todo })
    })
        .then(res => res.json())
        .then(res => {
            mapTodos(1);
            e.target.todo.value = "";
        })
        .catch(err => {
            addError("Could not add todo");
            console.log(err);
        });
};

const removeTodo = async (id) => {
  try {
    
    // Make the response
    const request = await fetch(`/api/todos/${id}`, { method: "DELETE" });
    const response = await request.json();

    // Refresh the list
    mapTodos(1);

  } catch (error) {
    addError('Failed to remove todo');
    console.log(error);
  }
}

mapTodos(1);
