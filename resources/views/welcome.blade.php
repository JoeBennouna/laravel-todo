<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito';
        }
    </style>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/todo.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
    <div class="container-fluid">
        <div id="errors" class="mt-3"></div>
        <h1 class="display text-center mt-3 mb-2">Todo List</h1>
        <form onsubmit="addTodo(event)">
            <div class="form-group">
                <input type="text" name="todo" class="form-control" id="exampleInputText" aria-describedby="emailHelp"
                    placeholder="Add a todo..">
            </div>
            <button type="submit" class="btn btn-primary w-100">Add Todo</button>
        </form>
        <ul class="list-group mt-4" id="todo-list">

        </ul>
        <div class="d-flex justify-content-center mt-5">
            <nav aria-label="Page navigation example">
                <ul class="pagination" id="pagination-nav">
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                </ul>
            </nav>
        </div>
    </div>
</body>

</html>